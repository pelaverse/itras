
<!-- README.md is generated from README.Rmd. Please edit that file -->

# iTRAS <img src="man/figures/iTRAS.png" align="right" alt="" width="150" />

<!-- badges: start -->
<!-- badges: end -->

The goal of {iTRAS} is to identify the individual strategies of fishing
vessels in the area of interest by analysis of high-resolution
geo-spatial data.

## Installation

You can install the development version of {iTRAS} like so:

``` r
install.packages("remotes")
remotes::install_gitlab(host = "https://gitlab.univ-lr.fr", repo = "pelaverse/itras")
library(iTRAS)
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
# library(iTRAS)
## basic example code
```

## Vignettes

Package {iTRAS} has vignettes to present the different functions
available.

``` r
vignette("convert-data", package = "iTRAS")
vignette("identifying-fishing-trips", package = "iTRAS")
vignette("identifying-fishing-operations", package = "iTRAS")
vignette("describing-fishing-operations", package = "iTRAS")
vignette("editing-graphics", package = "iTRAS")
```
